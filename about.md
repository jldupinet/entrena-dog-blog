---
title: Sobre Marisa
date: 2017-11-01 03:00:00 +0000
banner_image: "/uploads/2018/06/06/IMG_0099.jpg"
heading: Acerca de Marisa Vargas
sub_heading: ''
layout: landing-page
textline: ''
publish_date: 2017-12-01 04:00:00 +0000
show_staff: false
menu:
  footer:
    name: Sobre Marisa
    identifier: _about
    url: "/about/"
    weight: 3
  navigation:
    name: Sobre Marisa
    identifier: _about
    url: "/about/"
    weight: 2
---
Marisa Vargas es pasante de Medicina Veterinaria y Zootecnia por parte de la UNAM en la Ciudad de México, México. 

Tiene experiencia aplicando técnicas de etología y entrenamiento positivo para corregir conductas en mascotas sin necesidad de regaños ni experiencias negativas.

Actualmente brinda sesiones de entrenamiento privadas así como consultas en dos clínicas de la CDMX.

Si tienes dudas o deseas contratar sus servicios, escribe a marisa@entrena.dog